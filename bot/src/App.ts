import {io} from "socket.io-client"

import {RoleUpdateRequest,} from "./types";
import {Client, Discord, Once} from "discordx";
import {injectable} from "tsyringe";
import RoleUpdater from "./executions/RoleUpdater";

@Discord()
@injectable()
export default class App {

    private changeSocket = io("ws://localhost:8081")

    constructor(
        private client: Client
    ) {
    }

    @Once({event: 'ready'})
    private registerChangeListener() {
        this.changeSocket.on('change', args => this.onChange(args))
    }

    private async onChange(request: RoleUpdateRequest) {
        const roleUpdater = new RoleUpdater(request, this.client)
        await roleUpdater.update()
    }
}
