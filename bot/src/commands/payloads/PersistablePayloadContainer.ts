import PayloadContainer from "./PayloadContainer";
import fs from "fs";

export class PersistablePayloadContainer<T> extends PayloadContainer<T> {

    constructor(private readonly filename: string) {
        super();
    }

    public async fetch() {
        const fileContent = await fs.promises.readFile(
            this.payloadsFilePath,
            {encoding: "utf-8"}
        )

        this.data = JSON.parse(fileContent)
    }

    public async persist() {
        await fs.promises.writeFile(
            this.payloadsFilePath,
            JSON.stringify(this.data),
        )
    }

    private get payloadsFilePath() {
        return `${process.cwd()}/data/payloads/${this.filename}.json`
    }
}