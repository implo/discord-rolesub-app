export default class PayloadContainer<T> {

    protected data: Record<string, T> = {}

    public find(id: string): T {
        const payload = this.data[id]

        if (payload === undefined) {
            throw 'context not found'
        }

        return payload
    }

    public put(key: string, value: T) {
        this.data[key] = value
    }

}
