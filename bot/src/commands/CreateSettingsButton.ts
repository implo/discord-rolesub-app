import {ButtonComponent, Discord, Slash, SlashOption} from "discordx";
import {
    ActionRowBuilder,
    ButtonBuilder, ButtonInteraction,
    ButtonStyle,
    CommandInteraction, GuildMember, GuildMemberRoleManager, InteractionReplyOptions,
    MessageActionRowComponentBuilder
} from "discord.js";
import BackendService from "../services/BackendService";
import {injectable} from "tsyringe";
import {PersistablePayloadContainer} from "./payloads/PersistablePayloadContainer";

export interface CreateSettingsButtonPayload {
    id: string,
    setNo: number
    message: string
}

@Discord()
@injectable()
export default class CreateSettingsButton {

    private static buttonId = 'settings'
    private payloads = new PersistablePayloadContainer<CreateSettingsButtonPayload>(
        'create-settings-button'
    )

    constructor(
        private readonly backend: BackendService
    ) {
    }

    @Slash({ name: 'create-settings-button' })
    async execute(
        @SlashOption({name: 'set'})
        setNo: number,

        @SlashOption({name: 'message'})
        message: string,

        interaction: CommandInteraction
    ) {
        const options = CreateSettingsButton.createMessageOptions(message)
        const reply = await interaction.reply(options)

        this.payloads.put(reply.id, {
            id: reply.id,
            setNo,
            message
        })

        await this.payloads.persist()
    }

    private static createMessageOptions(content: string): InteractionReplyOptions {
        return {
            components: [this.createActionRow()],
            content,
            fetchReply: true
        }
    }

    private static createActionRow() {
        return new ActionRowBuilder<MessageActionRowComponentBuilder>()
            .addComponents(CreateSettingsButton.createButton())
    }

    private static createButton() {
        return new ButtonBuilder()
            .setLabel('Settings')
            .setStyle(ButtonStyle.Primary)
            .setEmoji('⚙')
            .setCustomId(CreateSettingsButton.buttonId)
    }

    @ButtonComponent({id: CreateSettingsButton.buttonId})
    async settingsClicked(interaction: ButtonInteraction) {
        await this.payloads.fetch()

        if (interaction.member === null) {
            throw 'member who clicked was null'
        }

        if (interaction.guild == null) {
            throw 'this only works in a guild'
        }

        const payload = this.payloads.find(interaction.message.id)
        const roleManager = interaction.member.roles as GuildMemberRoleManager
        const roles = roleManager.cache.map(role => role.name)

        const url = await this.backend.requestUrl(
            payload.setNo,
            interaction.member.user.id,
            interaction.guild.id,
            roles
        )

        const guildMember = interaction.member as GuildMember
        await guildMember.send(url)

        await interaction.update(payload.message)
    }
}