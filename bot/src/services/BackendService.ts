import {Discord} from "discordx";
import {injectable} from "tsyringe";
import axios from "axios";

@Discord()
@injectable()
export default class BackendService {

    private static url: string = "http://localhost"
    private static port: number = 8080

    private async post<T>(path: string, data: any): Promise<T> {
        return await axios.post(`${BackendService.url}:${BackendService.port}/${path}`, data)
            .then(result => result.data)
    }

    public async requestUrl(questionSet: number, userId: string, guildId: string, roles: string[]): Promise<string> {
        return await this.post(`create-url?set=${questionSet}&id=${userId}&guildId=${guildId}`, roles)
    }
}