import {Question, RoleUpdateRequest, RoleUpdateResponse} from "../types";
import {Client} from "discordx";
import {Colors, EmbedBuilder, EmbedField, Guild, GuildMember} from "discord.js";

export default class RoleUpdater {

    private guild!: Guild
    private member!: GuildMember
    private response: RoleUpdateResponse = RoleUpdater.emptyResult()

    constructor(
        private readonly request: RoleUpdateRequest,
        private readonly client: Client
    ) {
    }

    public async update() {
        await this.init()

        for (const result of this.request.results) {
            await this.tryUpdateRole(result)
        }

        await this.sendResponse()
    }

    private async init() {
        this.guild = await this.client.guilds.fetch(this.request.guildId)
        this.member = await this.guild.members.fetch(this.request.id)
    }

    public async tryUpdateRole(result: Question) {
        try {
            await this.updateRole(result)
        } catch (e) {
            console.log(e)
        }
    }

    public async updateRole(result: Question) {
        const roleObject = this.getRoleByName(result.role)

        if (result.answer) {
            await this.member.roles.add(roleObject.id)
            this.response.added.push(result.role)
        } else {
            await this.member.roles.remove(roleObject.id)
            this.response.removed.push(result.role)
        }

    }

    private getRoleByName(name: string) {
        const roleObject = this.guild.roles.cache.find(role => role.name === name)

        if (roleObject === undefined) {
            throw `role ${name} does not exitst. check the config`
        }

        return roleObject
    }

    private async sendResponse() {
        await this.member.send({embeds: [this.createEmbed()]})
    }

    private createEmbed() {
        const embed = new EmbedBuilder()
            .setColor(Colors.Green)
            .setTitle('Successfully updated Settings')

        if(this.addedEmbedField) {
            embed.addFields(this.addedEmbedField)
        }

        if(this.removedEmbedField) {
            embed.addFields(this.removedEmbedField)
        }

        return embed
    }

    private get addedEmbedField(): EmbedField | undefined {
        const roles = this.response.added
        return this.createListField('Subscribed', '- ✅', roles)
    }

    private get removedEmbedField(): EmbedField | undefined {
        const roles = this.response.removed
        return this.createListField('Not Subscribed', '- ❌', roles)
    }

    private createListField(title: string, prefix: string, roles: string[]): EmbedField | undefined {
        let stringBuilder = ''

        if (roles.length !== 0) {
            for (const role of roles) {
                stringBuilder += `${prefix} ${role} \n`
            }

            return {
                name: title,
                value: stringBuilder,
                inline: false,
            }
        } else {
            return undefined
        }
    }

    private static emptyResult(): RoleUpdateResponse {
        return {added: [], removed: []}
    }
}