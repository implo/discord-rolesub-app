import 'reflect-metadata'
import {Client, DIService, tsyringeDependencyRegistryEngine} from "discordx";
import {importx} from '@discordx/importer'
import * as dotenv from 'dotenv'
import {CommandInteraction, IntentsBitField} from "discord.js";
import {container} from "tsyringe"

import UserError from "./error/UserError";

let client: Client;

init().catch(console.log)

async function init() {
    initEnvironment()
    setupDI()
    client = createClient()
    registerClientInjectable()
    initCommands()
    await startBot()
}

function isProduction() {
    return process.env.NODE_ENV && process.env.NODE_ENV === 'production'
}

function initEnvironment() {
    if (!isProduction()) {
        dotenv.config({path: `${process.cwd()}/.env.dev`})
    }
}

function setupDI() {
    DIService.engine = tsyringeDependencyRegistryEngine
        .setInjector(container)
}

function createClient(): Client {
    if (isProduction()) {
        return getProductionClient()
    } else {
        return getDevelopmentClient()
    }
}

function getProductionClient(): Client {
    return new Client({
        intents: [
            IntentsBitField.Flags.Guilds,
            IntentsBitField.Flags.GuildMessages,
            IntentsBitField.Flags.GuildMembers
        ],
        silent: false,
    })
}

function getDevelopmentClient(): Client {
    return new Client({
        intents: [
            IntentsBitField.Flags.Guilds,
            IntentsBitField.Flags.GuildMessages,
            IntentsBitField.Flags.GuildMembers
        ],
        silent: false,
    })
}

function registerClientInjectable() {
    container.register<Client>(Client, {useValue: client})
}

function initCommands() {
    initCommandStructures()
    initCommandInteraction()
}

function initCommandStructures() {
    client.once('ready', async () => {
        await client.clearApplicationCommands()
        await client.initApplicationCommands()
    })
}

function initCommandInteraction() {
    client.on('interactionCreate', async (interaction) => {
        const commandInteraction = interaction as CommandInteraction

        try {
            await client.executeInteraction(interaction)
        } catch (e) {
            if (e instanceof UserError) {
                if (commandInteraction.replied) {
                    await commandInteraction.editReply({
                        content: "❌ " + e.message,
                    })
                } else {
                    await commandInteraction.reply({
                        content: "❌ " + e.message,
                        ephemeral: e.ephemeral
                    })
                }
                console.log(e)
            } else {
                console.log(e)
            }
        }
    })
}

async function startBot() {
    await importClasses()

    if (process.env.DISCORD_TOKEN) {
        await client.login(process.env.DISCORD_TOKEN)
    } else {
        throw new Error('Token is not defined. Please take a look into the' +
            '.env.dev or .env.prod file depending on your environment')
    }
}

async function importClasses() {
    await importx(
        __dirname + "/{events,commands,api}/**/*.js",
    )

    await importx(
        __dirname + "/App.js"
    )
}
