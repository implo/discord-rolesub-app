export default class UserError extends Error {

    constructor(msg: string, public ephemeral: boolean = false, public developMsg?: string) {
        super(msg);
    }

}