export interface Question {
    text: string,
    role: string
    allowedRoles: string
    answer: boolean
}

export interface RoleUpdateRequest {
    id: string
    guildId: string
    results: Question[]
}

export interface RoleUpdateResponse {
    added: string[]
    removed: string[]
}
