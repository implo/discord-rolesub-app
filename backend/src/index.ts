import express, {Response} from 'express'
import {Request} from 'express'
import { v4 } from 'uuid';
import {ChangeRequest, ConfigObject, PendingRequest, Question, QuestionSet, TypedRequestBody} from "./types";
import bodyParser from "body-parser"
import * as filesystem from 'fs'
import cors from 'cors'
import deepcopy from "deepcopy";
import {Server, Socket} from "socket.io";

const app = express()
const port = 8080;
let configObject: ConfigObject | undefined = undefined

const websocketConnection = new Server(8081);
let websocket: Socket | undefined = undefined

const pendingRequests: Map<string, PendingRequest> = new Map<string, PendingRequest>()

app.use(bodyParser.json())
app.use(cors())

app.get('/', (req, res) => {
    res.send('backend')
})

interface UserError extends Error {
    message: string
}

const sendError = (res: Response, error: UserError) => {
    console.error(error)
    res.status(404).send(error)
}


app.post('/create-url', (req: TypedRequestBody<string[]>, res) => {
    try {
        const uuid = v4()
        const setNo = getSetNoFromQueryParam(req)
        const id = getIdFromQueryParam(req)
        const guildId = getGuildIdFromQueryParam(req)
        const roles = req.body
        checkSetExistence(setNo)
        pendingRequests.set(uuid, {id, guildId, roles, setNo})

        res.send(
            `http://localhost:3000?uuid=${uuid}`
        )
    } catch (error) {
        sendError(res, error as UserError)
    }
})

function findPendingRequest(uuid: string) {
    const request = pendingRequests.get(uuid)

    if (request === undefined) {
        throw {
            message: 'pending request expired'
        }
    }

    return request
}

app.get('/questions', (req, res) => {
    try {
        const uuid = getUuidFromQueryParam(req)
        const request = findPendingRequest(uuid)
        const set = getSetBySetNo(request.setNo, request.roles)
        const filledSet = fillQuestionResults(set, request.roles)
        res.send(filledSet)
    } catch (error) {
        sendError(res, error as UserError)
    }
})

const questionSetToList = (qs: QuestionSet) =>  {
    const transformed: Question[] = []

    for (const category of Object.values(qs)) {
        for (const question of category) {
            transformed.push(question)
        }
    }

    return transformed
}

const sendToBot = (pendingRequest: PendingRequest, set: QuestionSet) => {
    const questionResults = questionSetToList(set)
    const changeRequest: ChangeRequest = {
        id: pendingRequest.id,
        guildId: pendingRequest.guildId,
        results: questionResults
    }

    if (websocket === undefined) {
        throw 'websocket was not inizialized yet'
    }

    websocket.emit('change', changeRequest)
}

app.post('/save', (req: TypedRequestBody<QuestionSet>, res) => {
    try {
        const uuid = getUuidFromQueryParam(req)
        sendToBot(findPendingRequest(uuid), deepcopy(req.body))
        pendingRequests.delete(uuid)
        res.send(req.body)
    } catch (error) {
        sendError(res, error as UserError)
    }
})

app.listen(port, async() => {
    console.log('express is listening to http://localhost:3000')
    await importQuestionSets()
})

const importQuestionSets = async () => {
    const projectPath = process.cwd()
    const result = await filesystem.promises.readFile(`${projectPath}/config/config.json`, "utf-8")
    configObject = JSON.parse(result)
}

const getSetNoFromQueryParam = (req: Request) => {
    const setNo = parseInt(req.query.set as string)

    if (setNo === undefined) {
        throw {
            message: 'set was not specified in query paramas'
        }
    }

    return setNo
}

const getIdFromQueryParam = (req: Request) =>  {
    const id = req.query.id as string

    if (id === undefined) {
        throw {
            message: 'id was not specified in query params'
        }
    }

    return id
}

const getUuidFromQueryParam = (req: Request) => {
    const uuid = req.query.uuid as string

    if (uuid === undefined) {
        throw {
            message: 'uuid was not specified'
        }
    }

    return uuid
}

const getGuildIdFromQueryParam = (req: Request) => {
    const guildId = req.query.guildId as string

    if (guildId === undefined) {
        throw {
            message: 'guild id was not specified'
        }
    }

    return guildId
}

const getConfigObject = () => {
    if (configObject === undefined) {
        throw {
            message: 'config object was not initialized yet'
        }
    }

    return deepcopy(configObject)
}

const checkSetExistence = (setNo: number) => {
    getSetBySetNo(setNo)
}

const getSetBySetNo = (setNo: number, roles: string[] = []) => {
    const set = getConfigObject().sets[setNo]

    for (const categoryName of Object.keys(set)) {
        const category = set[categoryName]
        const filteredCategory = category.filter(question => roles.includes(question.allowedRole))

        if (filteredCategory.length === 0) {
            delete set[categoryName]
        } else {
            set[categoryName] = filteredCategory
        }
    }

    if (set === undefined) {
        throw {
            message: 'set not found'
        }
    }

    return set
}

const fillQuestionResults = (qs: QuestionSet, roles: string[]) => {
    for (const category of Object.values(qs)) {
        for (const question of category) {
            if (roles.includes(question.role)) {
                question.answer = true
            }
        }
    }

    return qs
}

websocketConnection.on('connection', (socket) => {
    websocket = socket
})