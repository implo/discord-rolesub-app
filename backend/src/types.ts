import {Request} from "express";

export interface Question {
    text: string,
    role: string
    allowedRole: string
    answer: boolean
}

export type QuestionCategory = Array<Question>

export type QuestionSet = Record<string, QuestionCategory>

export interface ConfigObject {
    sets: Array<QuestionSet>
}

export interface TypedRequestBody<T> extends Request {
    body: T
}

export interface PendingRequest {
    id: string,
    guildId: string
    roles: string[]
    setNo: number
}

export interface ChangeRequest {
    id: string
    guildId: string
    results: Question[]
}

