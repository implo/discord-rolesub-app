export interface Question {
    text: string,
    role: string
    allowedRoles: string
    answer: boolean
}

export type QuestionCategory = Array<Question>

export type QuestionSet = Record<string, QuestionCategory>
