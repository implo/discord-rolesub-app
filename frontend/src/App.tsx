import './App.css'
import {MainContent} from "./MainContent";
import {QuestionSet} from "./question-set";
import axios from "axios";
import React, {useEffect, useState} from "react";

function App() {
    let [questionSet, setQuestionSet] = useState<QuestionSet | undefined>(undefined)
    let [isFinished, setFinished] = useState<boolean>(false)
    const uuid = getQueryParam<string>('uuid')

    if (uuid === undefined) {
        return (
            <div id="wrapper">
                <div className="error">Url is invalid</div>
            </div>
        )
    }

    useEffect(() => {
        fetchQuestions(uuid)
            .then(qs => setQuestionSet(qs))
    }, [])

    const save = (qs: QuestionSet) => {
        saveQuestions(uuid, qs)
            .catch(console.log)
        setFinished(true)
    }

    return (
        <>
            <div id='wrapper'>
                <Header/>
                <MainContent questionSet={questionSet} onSave={save} isFinished={isFinished} />
            </div>
        </>
    )
}

const Header = () => {
    return <header>
        <h1>Discord Roles</h1>
    </header>
}

export const fetchQuestions = (uuid: string): Promise<QuestionSet> => {
    return axios.get(`http://localhost:8080/questions?uuid=${uuid}`)
        .then(result => result.data)
}

export const getQueryParam = <T,>(key: string): T | undefined => {
    const paramAsStringOrNull = new URLSearchParams(window.location.search)
        .get(key)

    if (paramAsStringOrNull === null) {
        return undefined
    }

    try {
        return paramAsStringOrNull as unknown as T
    } catch (e) {
        return undefined
    }
}

export const saveQuestions = async (uuid: string, questionSet: QuestionSet) => {
    await axios.post(
        `http://localhost:8080/save?uuid=${uuid}`,
        questionSet
    )
}

export default App
