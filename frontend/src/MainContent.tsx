import {QuestionSetForm} from "./QuestionSetForm";
import {QuestionSet} from "./question-set";

import './MainContent.css'

export interface MainContentProps {
    questionSet?: QuestionSet
    onSave?: (questionSet: QuestionSet) => void
    onCancel?: () => void
    isFinished?: boolean
}

export function MainContent({ questionSet, onSave, onCancel, isFinished}: MainContentProps) {
    const questionSetHasCategories = (qs: QuestionSet) => Object.keys(qs).length > 0

    const ContentSwitcher = () => {
        if (questionSet === undefined) {
            return <QuestionSetInvalidError/>
        } else if (!questionSetHasCategories(questionSet)) {
            return <NoContentError/>
        } else if (isFinished) {
            return <SaveMessage/>
        } else {
            return <QuestionSetForm
                questionSet={questionSet}
                onSubmit={onSave}
                onCancel={onCancel}
            />
        }
    }

    return (
        <div data-testid="main-content">
            <ContentSwitcher/>
        </div>
    )
}

function QuestionSetInvalidError() {
    return (
        <div data-testid="question-set-validation">
            Request expired. Generate a new url.
        </div>
    )
}

function NoContentError() {
    return (
        <div data-testid="question-set-validation">
            There are no channels to subscribe to
        </div>
    )
}

function SaveMessage() {
    return (
        <div data-testid="save-message">
            Successfully Updated your Settings. You can close this Tab now
        </div>
    )
}
