import {Question, QuestionSet} from "./question-set";
import './QuestionSetForm.css'
import Switch from 'react-switch'
import {useState} from "react";

export interface QuestionSetFormProps {
    questionSet: QuestionSet
    onSubmit?: (questionSet: QuestionSet) => void
    onCancel?: () => void
}

export function QuestionSetForm({questionSet, onSubmit, onCancel}: QuestionSetFormProps) {

    const save = () => {
        if (onSubmit) {
            onSubmit(questionSet)
        }
    }

    const cancel = () => {
        if (onCancel) {
            onCancel()
        }
    }


    return (
        <>
            <div data-testid="question-set-form">
                {Object.keys(questionSet).map(category => {
                    return questionSet[category].length !== 0
                        ? <QuestionCategory questions={questionSet[category]} title={category} key={category} />
                        : <></>
                })}
            </div>
            <div className='button-navigation'>
                <div className='container'>
                    <button className='button-red' data-testid="cancel-button" onClick={() => cancel()}>Cancel</button>
                    <button className='button-green' data-testid="save-button" onClick={() => save()}>Save</button>
                </div>
            </div>
        </>
    )
}

interface QuestionCategoryProps {
    title: string
    questions: Question[]
}

function QuestionCategory({title, questions}: QuestionCategoryProps) {
    const inputId = (questionText: string) => `input-${questionText}`

    return (
        <div data-testid="question-category" className='question-category'>
            <h2 data-testid="category-heading">{title}</h2>
            <div className='question-category-content'>
                {questions.map(question => {
                    const [check, setCheck] = useState(question.answer)

                    return (
                        <div className="question" key={question.role} data-testid="question" >
                            <label htmlFor={inputId(question.role)}>{question.text}</label>
                            <QuestionSwitch
                                id={inputId(question.role)}
                                checked={check}
                                onChange={change => {
                                    setCheck(change)
                                    question.answer = change
                                }}
                            />
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

interface QuestionSwitchProps {
    id: string,
    checked: boolean
    onChange: (checked: boolean) => void,
}

function QuestionSwitch({id, checked, onChange}: QuestionSwitchProps) {
    return <Switch
        id={id}
        checked={checked}
        onChange={onChange}
        onColor="#86d3ff"
        onHandleColor="#2693e6"
        handleDiameter={30}
        uncheckedIcon={false}
        checkedIcon={false}
        boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
        activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
        height={20}
        width={48}
    />
}