import {fireEvent, render} from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect'
import {QuestionSetForm, QuestionSetFormProps} from "../src/QuestionSetForm";
import {Question} from "../src/question-set";

describe('<QuestionSetForm/>', () => {

    function question(number: number = 0): Question{
        return {
            text: `Dummy${number}`,
            allowedRoles: `@dummy${number}`,
            role: `@dummy${number}`,
            answer: false
        }
    }

    function renderQuestionSetForm(overrideProps: Partial<QuestionSetFormProps> = {}) {
        const defaultProps: QuestionSetFormProps = {
            questionSet: {
                default: [
                    question(0),
                    question(1),
                    question(2)
                ]
            },
            onSubmit: undefined,
            onCancel: undefined
        }

        const props = {...defaultProps, ...overrideProps}
        return render(<QuestionSetForm {...props} />)
    }

    it('renders one category headline', () => {
        const questionSetForm = renderQuestionSetForm()
        expect(questionSetForm.getByTestId('category-heading'))
            .toHaveTextContent('default')
    });

    it('renders two category headlines', () => {
        const questionSetForm = renderQuestionSetForm({
            questionSet: {
                default: [
                    question(0),
                    question(1),
                ],
                premium: [
                    question(2)
                ]
            }
        })

        expect(questionSetForm.getAllByTestId('category-heading'))
            .toHaveLength(2)

        expect(questionSetForm.getByText('default').tagName)
            .toEqual('H2')

        expect(questionSetForm.getByText('premium').tagName)
            .toEqual('H2')
    });

    it('renders 1 category with 3 questions', () => {
        const questionSetForm = renderQuestionSetForm()
        expect(questionSetForm.getAllByTestId('question'))
            .toHaveLength(3)
    })

    it('renders 1 category with 3 question with title and checkbox', () => {
        const questionSetForm = renderQuestionSetForm()
        const checkQuestionByNumber = (number: number) => {
            expect(questionSetForm.getAllByTestId('question')[number])
                .toHaveTextContent(`Dummy${number}`)

            expect(questionSetForm.getAllByTestId('question')[number].getElementsByTagName('input'))
                .toHaveLength(1)
            expect(questionSetForm.getAllByTestId('question')[number].querySelector('input'))
                .toHaveAttribute('type', 'checkbox')
            expect(questionSetForm.getAllByTestId('question')[number].querySelector('input'))
                .toHaveAttribute('type', 'checkbox')
            expect(questionSetForm.getAllByTestId('question')[number].querySelector('label'))
                .toHaveAttribute('for', `input-@dummy${number}`)
            expect(questionSetForm.getAllByTestId('question')[number].querySelector('input'))
                .toHaveAttribute('id', `input-@dummy${number}`)
        }

        checkQuestionByNumber(0)
        checkQuestionByNumber(1)
        checkQuestionByNumber(2)
    })

    it('calls onSubmit callback when save button is called', () => {
        const mockCallback = jest.fn()
        const questionSetForm = renderQuestionSetForm({onSubmit: mockCallback})
        const saveButton = questionSetForm.getByTestId('save-button')

        if (saveButton === null) {
            throw Error("save Button does not exist")
        }

        fireEvent.click(saveButton)
        expect(mockCallback).toBeCalledTimes(1)

        fireEvent.click(saveButton)
        fireEvent.click(saveButton)

        expect(mockCallback).toBeCalledTimes(3)
    })

   it('calls onCancel when the cancel button is called', () => {
       const mockCallback = jest.fn()
       const questionSetForm = renderQuestionSetForm({onCancel: mockCallback})
       const cancelButton = questionSetForm.getByTestId('cancel-button')

       if (cancelButton === null) {
           throw Error("save Button does not exist")
       }

       fireEvent.click(cancelButton)
       expect(mockCallback).toBeCalledTimes(1)

       fireEvent.click(cancelButton)
       fireEvent.click(cancelButton)

       expect(mockCallback).toBeCalledTimes(3)
   })
});