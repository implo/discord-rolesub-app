import {fireEvent, render} from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect'

import {MainContent, MainContentProps} from "../src/MainContent";

describe('<MainContent>',() => {

    const renderComponent = (overrideProps: Partial<MainContentProps> = {}) => {
        const defaultProps: MainContentProps = {
            questionSet: {
                default: [
                    {
                        text: 'Dummy',
                        allowedRoles: '@dummy',
                        answer: false,
                        role: '@dummy',
                    },
                ]
            },
            onCancel: () => undefined,
            onSave: _ => undefined,
            isFinished: false
        }

        const props = {...defaultProps, ...overrideProps}
        return render(<MainContent {...props} />)
    }

    it('renders error when no questions where retreived', function () {
        const mainContent = renderComponent({questionSet: undefined})
        expect(mainContent.getByTestId('question-set-validation'))
            .toHaveTextContent('Request expired. Generate a new url')
    });


    it('renders error when no questions are present', () => {
        const mainContent = renderComponent({questionSet: {}})
        expect(mainContent.getByTestId('question-set-validation'))
            .toHaveTextContent('There are no channels to subscribe to')
    });

    it('renders saved message when isFinished is passed', () => {
        const mainContent = renderComponent({ isFinished: true })

        expect(mainContent.getByTestId('save-message'))
            .toHaveTextContent('Successfully Updated your Settings. You can close this Tab now')
    })

    it('renders Questions when set is valid', () => {
        const mainContent = renderComponent()
        expect(mainContent.queryByTestId('question-set-form')).not.toBeNull()
    })

    it('does not render Error when questions are present', () => {
        const mainContent = renderComponent()
        expect(mainContent.queryByTestId('question-set-validation')).toBeNull()
    });


    it("calls event when save button is clicked", () => {
        const mockCallback = jest.fn()
        const mainContent = renderComponent({onSave: mockCallback})
        const saveButton = mainContent.getByTestId('save-button')

        if (saveButton === null) {
            throw Error("save Button does not exist")
        }

        fireEvent.click(saveButton)
        expect(mockCallback).toBeCalledTimes(1)

        fireEvent.click(saveButton)
        fireEvent.click(saveButton)

        expect(mockCallback).toBeCalledTimes(3)
    })

    it('calls onCancel when the cancel button is called', () => {
        const mockCallback = jest.fn()
        const mainContent = renderComponent({onCancel: mockCallback})
        const cancelButton = mainContent.getByTestId('cancel-button')

        if (cancelButton === null) {
            throw Error("save Button does not exist")
        }

        fireEvent.click(cancelButton)
        expect(mockCallback).toBeCalledTimes(1)

        fireEvent.click(cancelButton)
        fireEvent.click(cancelButton)

        expect(mockCallback).toBeCalledTimes(3)
    })

});

